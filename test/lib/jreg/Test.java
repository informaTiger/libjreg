package lib.jreg;

import lib.jreg.entity.EntryType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author thomas
 */
public class Test {
    
    public static void main(String[] args) throws Exception{
        Registry reg = Registry.getInstance();
        
        reg.put(EntryType.REG_SZ, "HKEY_LOCAL_MACHINE/myDict/home", "/home/thomas");
        reg.put(EntryType.REG_DWORD, "HKEY_LOCAL_MACHINE/myDict/serialID", 10);
        reg.put(EntryType.REG_QWORD, "HKEY_LOCAL_MACHINE/autobackup/run", -1L);
        reg.put(EntryType.REG_BINARY, "HKEY_LOCAL_MACHINE/bitbucket/raw/logo", new byte[]{ 12, -2, 25, 116, 28, 78, 34 });
        
        reg.commit();
        
        System.out.println(reg.get("HKEY_LOCAL_MACHINE/myDict/serialID"));
        System.out.println(reg.get("HKEY_LOCAL_MACHINE/bitbucket/raw/logo"));        
    }
}
