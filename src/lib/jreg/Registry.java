/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg;

import java.io.File;
import java.nio.file.Paths;
import java.util.Stack;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import lib.jreg.entity.Entry;
import lib.jreg.entity.EntryType;
import lib.jreg.entity.Path;
import lib.jreg.entity.reader.EntryReader;
import lib.jreg.entity.writer.EntryWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class Registry {
    
    public static final String REGISTRY_PATH;
    
    private static final XPath XPATH;
    
    private static Registry instance = null;
    
    private final Stack<Entry> buffer;
    
    private Document document;
    
    private Registry() throws Exception{
        buffer = new Stack<>();
        
        File reg = new File(REGISTRY_PATH);
        if (!reg.exists()){
            reg.createNewFile();
            commit();
        }        
        load(REGISTRY_PATH);
    }
    
    static {
        REGISTRY_PATH = Paths.get(
            System.getProperty("user.home"),
            ".jreg"
        ).toString();
        
        XPATH = XPathFactory
            .newInstance()
            .newXPath();
    }
    
    public final void load(String path) throws Exception{        
        DocumentBuilder documentBuilder = DocumentBuilderFactory
            .newInstance()
            .newDocumentBuilder();

        document = documentBuilder.parse(new File(path));
    }
    
    public final void commit() throws Exception{
        commit(REGISTRY_PATH);
    }
    
    public final void commit(String path) throws Exception{
        Element root;
        
        if (document == null){
            DocumentBuilder documentBuilder = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder();
            document = documentBuilder.newDocument();
            
            root = document.createElement("registry");
            document.appendChild(root);
        } else {
            root = document.getDocumentElement();
        }
        
        Entry e;
        while (!buffer.empty()){
            e = buffer.pop();
            
            XPathExpression expr = XPATH.compile(String.format(
                    "//entry[@path='%s']",
                    e.getKey().getValue()
                )
            );
            Element elem = (Element)expr.evaluate(document, XPathConstants.NODE);
            
            if (elem == null){
                elem = document.createElement("entry");
            }
            elem.setAttribute("type", e.getType().name());
            elem.setAttribute("path", e.getKey().getValue());

            EntryWriter writer = EntryWriter.Registry.get(e.getType().getTarget());
            writer.write(e.getValue(), elem);

            root.appendChild(elem);
        }
        Transformer transformer = TransformerFactory
            .newInstance()
            .newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(path));
        
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        transformer.transform(source, result);
    }
    
    public Entry get(Path key) throws Exception{
        XPathExpression expr = XPATH.compile(String.format(
                "//entry[@path='%s']",
                key.getValue()
            )
        );
        Element element = (Element)expr.evaluate(document, XPathConstants.NODE);
        
        if (element == null){
            throw new IllegalArgumentException(String.format(
                    "No such key %s",
                    key.getValue()
                )
            );
        }
        EntryType type = EntryType.valueOf(element.getAttribute("type"));        
        Entry entry = new Entry(type, key);
        
        EntryReader reader = EntryReader.Registry.get(type.getTarget());
        entry.setValue(reader.read(element));
        
        return entry;
    }
    
    public Entry get(String key) throws Exception{
        return get(new Path.Builder()
            .push(key)
            .build()
        );
    }
    
    public void put(Entry entry){
        buffer.push(entry);
    }
    
    public void put(EntryType type, String path, Object value){
        Entry entry = new Entry(
            type,
            new Path.Builder()
                .push(path)
                .build()
        );
        entry.setValue(value);
        
        put(entry);
    }
    
    public final void clear(){
        buffer.clear();
    }
    
    public static Registry getInstance() throws Exception{
        if (instance == null){
            instance = new Registry();
        }
        return instance;
    }
}
