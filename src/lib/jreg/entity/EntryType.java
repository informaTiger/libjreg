/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity;

/**
 *
 * @author thomas
 */
public enum EntryType {
    
    REG_BINARY(byte[].class), REG_DWORD(Integer.class), REG_LINK(Entry.class), REG_SZ(String.class), REG_QWORD(Long.class);
    
    private final Class target;
    
    EntryType(Class target){
        this.target = target;
    }
    
    public Class getTarget(){
        return target;
    }
}
