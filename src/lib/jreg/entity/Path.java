/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity;

import java.util.Objects;

/**
 *
 * @author thomas
 */
public class Path {
 
    public static final String PATH_SEPARATOR = "/";
    
    /*
       recursive pattern (not supported by Java): ^[a-zA-Z](?>(?>[a-zA-Z0-9_]+\\/|(?R))+)$
       old pattern: ^([a-zA-Z])([a-zA-Z0-9\\/_]+)$
    */
    public static final String REGEX_PATTERN = "^[a-zA-Z](?>(?>[a-zA-Z0-9_]+\\/|[a-zA-Z0-9_]+\\/)+)$";
    
    private final String value;
    
    private Path(Builder builder){
        String value = builder.path.toString();
        
        if (!value.matches(REGEX_PATTERN)){
            throw new IllegalArgumentException(String.format(
                    "Value %s is not a valid path",
                    value
                )
            );
        }        
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static class Builder {
     
        private StringBuilder path;
        
        public Builder(){
            path = new StringBuilder();
        }
        
        public Builder push(String value){
            path.append(value);
            
            if (!value.endsWith(PATH_SEPARATOR)){
                path.append(PATH_SEPARATOR);
            }
            return this;
        }
        
        public Path build(){
            return new Path(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Path other = (Path) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return value;
    }
}
