/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.reader;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class StringReader implements EntryReader<String> {

    @Override
    public String read(Element element) {
        return element.getTextContent();
    }
}
