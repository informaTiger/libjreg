/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.reader;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public abstract class NumberReader<T extends Number> implements EntryReader<T> {
    
    private boolean negative;
    
    public final String preProcess(Element element){
        String value = element.getTextContent();
        
        if (value.startsWith("-")){
            value = value.substring(1);
            negative = true;
        }
        return value;
    }
    
    public final Number postProcess(T n){
        if (negative){    
            return n.longValue() * -1;
        }
        return n.longValue();
    }
}
