/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.reader;

import java.util.Base64;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class ByteReader implements EntryReader<byte[]> {

    @Override
    public byte[] read(Element element) {
        return Base64.getDecoder()
            .decode(element.getTextContent());
    }
}
