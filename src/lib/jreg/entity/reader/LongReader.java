/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.reader;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class LongReader extends NumberReader<Long> {
    
    @Override
    public Long read(Element element) {
        long res = Long.parseLong(preProcess(element), 16);
        return postProcess(res).longValue();
    }
}
