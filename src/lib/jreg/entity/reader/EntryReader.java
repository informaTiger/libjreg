/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.reader;

import java.util.HashMap;
import java.util.Map;
import lib.jreg.entity.Entry;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public interface EntryReader<T> {
    
    public static class Registry {
        
        private static final Map<Class, EntryReader> REGISTRY;
        
        static {
            REGISTRY = new HashMap<>();
            initDefaults();
        }
        
        private static void initDefaults(){
            register(String.class, new StringReader());
            register(byte[].class, new ByteReader());
            register(Long.class, new LongReader());
            register(Integer.class, new IntegerReader());
            register(Entry.class, new EntryReaderImpl());
        }
        
        public static <T> void register(Class<T> type, EntryReader<T> reader){
            if (type == null || reader == null){
                throw new NullPointerException();
            }
            REGISTRY.put(type, reader);
        }
        
        public static <T> EntryReader<T> get(Class<T> type){
            EntryReader<T> reader = REGISTRY.get(type);
            
            if (reader == null){
                throw new IllegalArgumentException(String.format(
                        "No reader registered for class %s",
                        type.getSimpleName()
                    )                
                );
            }
            return reader;
        }
    }
    
    public T read(Element element);
}
