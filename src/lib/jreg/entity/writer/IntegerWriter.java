/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class IntegerWriter extends NumberWriter<Integer> {

    @Override
    public void write(Integer data, Element element) {
        data = preProcess(data).intValue();
        element.setTextContent(getPrefix() + Integer.toHexString(data));
    }
}
