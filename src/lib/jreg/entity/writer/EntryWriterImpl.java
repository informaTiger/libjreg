/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import lib.jreg.entity.Entry;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class EntryWriterImpl implements EntryWriter<Entry> {

    @Override
    public void write(Entry data, Element element) {
        element.setTextContent(data.getKey().getValue());
    }
}
