/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import lib.jreg.entity.Entry;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public interface EntryWriter<T> {
    
    public static class Registry {
        
        private static final Map<Class, EntryWriter> REGISTRY;
        
        static {
            REGISTRY = new HashMap<>();
            initDefaults();
        }
        
        private static void initDefaults(){
            register(String.class, new StringWriter());
            register(byte[].class, new ByteWriter());
            register(Long.class, new LongWriter());
            register(Integer.class, new IntegerWriter());
            register(Entry.class, new EntryWriterImpl());
        }
        
        public static <T> void register(Class<T> type, EntryWriter<T> writer){
            if (type == null || writer == null){
                throw new NullPointerException();
            }
            REGISTRY.put(type, writer);
        }
        
        public static <T> EntryWriter<T> get(Class<T> type){
            EntryWriter writer = REGISTRY.get(type);
            
            if (writer == null){
                throw new IllegalArgumentException(String.format(
                        "No writer registered for entry type %s",
                        type.getSimpleName()
                    )
                );
            }
            return writer;
        }
    }
    
    public void write(T data, Element element);
}
