/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class StringWriter implements EntryWriter<String> {

    @Override
    public void write(String data, Element element) {
        element.setTextContent(data);
    }
}
