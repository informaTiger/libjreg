/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import java.util.Base64;
import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class ByteWriter implements EntryWriter<byte[]>{

    @Override
    public void write(byte[] data, Element element) {
        element.setTextContent(Base64.getEncoder()
            .encodeToString(data)
        );
    }
}
