/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

import org.w3c.dom.Element;

/**
 *
 * @author thomas
 */
public class LongWriter extends NumberWriter<Long> {

    @Override
    public void write(Long data, Element element) {
        data = preProcess(data).longValue();
        element.setTextContent(getPrefix() + Long.toHexString(data));
    }
}
