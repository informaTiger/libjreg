/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity.writer;

/**
 *
 * @author thomas
 */
public abstract class NumberWriter<T extends Number> implements EntryWriter<T> {
    
    private boolean negative;
    
    public final Number preProcess(T n){
        if (n.longValue() < 0){
            negative = true;
            return n.longValue() * -1;
        }
        return n.longValue();
    }
    
    public final String getPrefix(){
        return negative ? "-" : "";
    }
}
