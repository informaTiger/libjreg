/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.jreg.entity;

/**
 *
 * @author thomas
 */
public class Entry {

    private final EntryType type;
    
    private final Path key;
    
    private Object value;
    
    public Entry(EntryType type, Path key){
        this.type = type;
        this.key = key;
    }
    
    public EntryType getType(){
        return type;
    }
    
    public Path getKey(){
        return key;
    }
    
    public Object getValue(){
        return value;
    }
   
    public void setValue(Object value){
        if (type.getTarget().isInstance(value)){
            this.value = value;
        } else{
            throw new IllegalArgumentException(String.format(
                    "Value %s is not valid for entry type %s",
                    value,
                    type
                )
            );
        }
    }
    
    @Override
    public String toString(){
        return key + " => " + value;
    }
}
